import { StyleSheet } from "react-native";
export default StyleSheet.create({
  fieldSection: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    borderColor: "black",
    borderBottomWidth: 1,
  },
  icon: {
    paddingTop: 10,
    paddingBottom: 10,
  },
  input: {
    paddingLeft: 10,
    paddingTop: 10,
    paddingBottom: 10,
    width: "80%",
  },
});
