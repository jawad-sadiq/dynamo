import "react-native-gesture-handler";
import React, { Component } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { connect } from "react-redux";
import Home from "../screens/home";
import Login from "../screens/login";
import Signup from "../screens/signup";
import Splash from "../screens/splash";

const Stack = createStackNavigator();
class Navigation extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          {this.props.isSignedIn === "true" ? (
            <Stack.Screen name="Home" component={Home} />
          ) : this.props.isLoading ? (
            <Stack.Screen name="Splash" component={Splash} options={{ headerShown: false }} />
          ) : (
            <>
              <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
              <Stack.Screen
                name="Signup"
                component={Signup}
                options={{
                  headerStyle: {
                    elevation: 0, // remove shadow on Android
                    shadowOpacity: 0,
                  },
                }}
              />
            </>
          )}
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoading: state.isLoading,
    isSignedIn: state.isSignedIn,
  };
}

export default connect(mapStateToProps)(Navigation);
