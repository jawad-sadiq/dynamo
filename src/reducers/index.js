import { Types } from "../constants/actionTypes";
import AsyncStorage from "@react-native-async-storage/async-storage";

const initialState = {
  user: {
    name: "",
    email: "",
    password: "",
    confirm_password: "",
  },
  isLoading: true,
  isSignedIn: "false",
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.REGISTER_USER:
      hard_storage("REGISTER_USER", action.payload);
      return {
        ...state,
        user: action.payload,
      };
    case Types.IS_LOADING:
      return {
        ...state,
        isLoading: action.payload,
      };
    case Types.IS_SIGNED_IN:
      return {
        ...state,
        isSignedIn: action.payload,
      };
    default:
      return state;
  }
};

const hard_storage = async (action, data) => {
  switch (action) {
    case "REGISTER_USER":
      await AsyncStorage.setItem("name", data.user.name);
      await AsyncStorage.setItem("email", data.user.email);
      break;
    default:
      break;
  }
};

export default reducer;
