import React, { Component } from "react";
import { Text, View, TouchableOpacity, AsyncStorage } from "react-native";
import { connect } from "react-redux";
import { Types } from "../constants/actionTypes";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        name: "",
        email: "",
      },
    };
  }

  //Load data from storage
  componentDidMount = async () => {
    this.setState({
      user: {
        ...this.state.user,
        name: await AsyncStorage.getItem("name"),
        email: await AsyncStorage.getItem("email"),
      },
    });
  };

  signOut = async () => {
    await AsyncStorage.setItem("isSignedIn", "false");
    this.props.isSignedIn(await AsyncStorage.getItem("isSignedIn"));
  };

  render() {
    return (
      <>
        <Text>Name: {this.state.user.name}</Text>
        <Text>Email: {this.state.user.email}</Text>
        <TouchableOpacity onPress={() => this.signOut()} activeOpacity={0.6}>
          <Text style={{ fontSize: 15, color: "orange" }}>Sign Out</Text>
        </TouchableOpacity>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    isSignedIn: state.isSignedIn,
    user: state.user,
  };
};

export const mapDispatchToProps = (dispatch) => {
  return {
    isSignedIn: (status) => dispatch({ type: Types.IS_SIGNED_IN, payload: status }),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Home);
