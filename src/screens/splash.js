import React, { Component } from "react";
import { ImageBackground, View } from "react-native";
import style from "../styles/splash";
import { connect } from "react-redux";
import { Types } from "../constants/actionTypes";
import AsyncStorage from "@react-native-async-storage/async-storage";

class Splash extends Component {
  constructor(props) {
    super(props);
  }

  handleAction = async () => {
    //Hide splash after 3sec
    setTimeout(() => {
      this.props.isLoading(false);
    }, 5000);

    //After app refresh check user was signed in or not.
    //Set signedIn into redux
    this.props.isSignedIn(await AsyncStorage.getItem("isSignedIn"));
  };

  render() {
    this.handleAction();
    return (
      <View style={style.container}>
        <ImageBackground style={style.splash} source={require("../../assets/logo.png")} />
      </View>
    );
  }
}

export const mapDispatchToProps = (dispatch) => {
  return {
    isLoading: (param) => dispatch({ type: Types.IS_LOADING, payload: param }),
    isSignedIn: (param) => dispatch({ type: Types.IS_SIGNED_IN, payload: param }),
  };
};

export default connect(null, mapDispatchToProps)(Splash);
