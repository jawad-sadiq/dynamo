import React, { Component } from "react";
import { Text, TextInput, View, SafeAreaView, TouchableOpacity, TouchableWithoutFeedback, Keyboard, AsyncStorage, ScrollView } from "react-native";
import { KeyboardAvoidingScrollView } from "react-native-keyboard-avoiding-scroll-view";
import styles from "../styles/login";
import Icon from "react-native-vector-icons/FontAwesome";
import Toast from "../components/flash";
import { connect } from "react-redux";
import { Types } from "../constants/actionTypes";
import { LinearGradient } from "expo-linear-gradient";

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        name: "",
        email: "",
        password: "",
        confirm_password: "",
      },
      show_toast: false,
      message: "",
      showFooter: true,
    };
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", this._keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = () => {
    this.setState({ showFooter: false });
  };

  _keyboardDidHide = () => {
    this.setState({ showFooter: true });
  };

  handleAction = async (actionType, text = null) => {
    switch (actionType) {
      case "NAME_INPUT":
        this.setState({
          user: {
            ...this.state.user,
            name: text,
          },
        });

        break;
      case "EMAIL_INPUT":
        this.setState({
          user: {
            ...this.state.user,
            email: text,
          },
        });

        break;
      case "PASSWORD_INPUT":
        this.setState({
          user: {
            ...this.state.user,
            password: text,
          },
        });

        break;
      case "CONFIRM_PASSWORD_INPUT":
        this.setState({
          user: {
            ...this.state.user,
            confirm_password: text,
          },
        });

        break;
      case "SIGNUP":
        if (this.state.user.password === this.state.user.confirm_password) {
          this.props.registerUser(this.state.user);
          try {
            await AsyncStorage.setItem(this.state.user.email, this.state.user.password);
          } catch (error) {
            this.setState({ show_toast: true, message: "Sorry, account not created. Try again!" });
          }
          this.props.navigation.navigate("Login");
        } else {
          this.setState({ show_toast: true, message: "Password and confirm password doesn't matched" });
        }
        break;
      case "GO_TO_LOGIN":
        this.props.navigation.navigate("Login");
        break;
      default:
        break;
    }
  };

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: "center", backgroundColor: "red" }}>
          <SafeAreaView style={styles.container}>
            <View style={styles.loginTitleContainer}>
              <Text style={styles.loginTitle}>Create an account</Text>
            </View>
            <View style={styles.fieldsContainer}>
              <View style={styles.field}>
                <Text style={styles.iconContainer}>
                  <Icon name="user" style={styles.icon} size={15} color="black" />
                </Text>
                <TextInput
                  name="name"
                  style={[styles.input, { width: "100%" }]}
                  onChangeText={(text) => {
                    this.handleAction("NAME_INPUT", text);
                  }}
                  value={this.state.user.name}
                  returnKeyType="next"
                  returnKeyLabel="next"
                  onSubmitEditing={() => {
                    this.refs.email.focus();
                  }}
                  autoCapitalize="none"
                  placeholder="Name"
                  blurOnSubmit={false}
                />
              </View>

              <View style={styles.field}>
                <Text style={styles.iconContainer}>
                  <Icon name="envelope" style={styles.icon} size={12} color="black" />
                </Text>
                <TextInput
                  name="email"
                  ref="email"
                  style={[styles.input, { width: "100%" }]}
                  onChangeText={(text) => {
                    this.handleAction("EMAIL_INPUT", text);
                  }}
                  value={this.state.user.email}
                  returnKeyType="next"
                  returnKeyLabel="next"
                  onSubmitEditing={() => {
                    this.refs.pass.focus();
                  }}
                  autoCapitalize="none"
                  placeholder="Email"
                  blurOnSubmit={false}
                />
              </View>

              <View style={styles.field}>
                <Text style={styles.iconContainer}>
                  <Icon name="lock" style={styles.icon} size={15} color="black" />
                </Text>
                <TextInput
                  name="password"
                  ref="pass"
                  style={[styles.input, { width: "100%" }]}
                  onChangeText={(text) => {
                    this.handleAction("PASSWORD_INPUT", text);
                  }}
                  returnKeyType="done"
                  returnKeyLabel="done"
                  onSubmitEditing={() => {
                    this.refs.confirm_pass.focus();
                  }}
                  value={this.state.user.password}
                  autoCapitalize="none"
                  placeholder="Password"
                  secureTextEntry={true}
                  blurOnSubmit={false}
                />
              </View>

              <View style={styles.field}>
                <Text style={styles.iconContainer}>
                  <Icon name="lock" style={styles.icon} size={15} color="black" />
                </Text>
                <TextInput
                  name="confirm_password"
                  ref="confirm_pass"
                  style={[styles.input, { width: "100%" }]}
                  onChangeText={(text) => {
                    this.handleAction("CONFIRM_PASSWORD_INPUT", text);
                  }}
                  returnKeyType="done"
                  returnKeyLabel="done"
                  onSubmitEditing={() => {
                    this.handleAction("SIGNUP");
                  }}
                  value={this.state.user.confirm_password}
                  autoCapitalize="none"
                  placeholder="Confirm Password"
                  secureTextEntry={true}
                  blurOnSubmit={false}
                />
              </View>
            </View>

            <View style={styles.loginButtonContainer}>
              <TouchableOpacity onPress={() => this.handleAction("SIGNUP")}>
                <LinearGradient colors={["#FFA500", "#ffdb99"]} style={styles.loginButton}>
                  <Text style={{ fontSize: 15, color: "white" }}>Signup</Text>
                  <Icon name="arrow-right" style={[styles.icon, { paddingLeft: 10, color: "white" }]} size={15} />
                </LinearGradient>
              </TouchableOpacity>
            </View>

            {this.state.showFooter && (
              <View style={styles.footer}>
                <Text>Already have an account? </Text>
                <TouchableOpacity onPress={() => this.handleAction("GO_TO_LOGIN")} activeOpacity={0.6}>
                  <Text style={{ fontSize: 15, color: "orange" }}>Login</Text>
                </TouchableOpacity>
              </View>
            )}
            <Toast visible={this.state.show_toast} message={this.state.message} />
          </SafeAreaView>
        </ScrollView>
      </TouchableWithoutFeedback>
    );
  }
}

export const mapDispatchToProps = (dispatch) => {
  return {
    registerUser: (user) => dispatch({ type: Types.REGISTER_USER, payload: { user } }),
  };
};

const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Signup);
