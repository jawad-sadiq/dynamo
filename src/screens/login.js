import React, { Component } from "react";
import { Text, TextInput, View, SafeAreaView, TouchableOpacity, TouchableWithoutFeedback, Keyboard, AsyncStorage } from "react-native";
import styles from "../styles/login";
import Icon from "react-native-vector-icons/FontAwesome";
import Toast from "../components/flash";
import { connect } from "react-redux";
import { Types } from "../constants/actionTypes";
import { LinearGradient } from "expo-linear-gradient";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        email: "",
        password: "",
      },
      show_toast: false,
      message: "",
    };
  }

  handleAction = async (actionType, text = null) => {
    switch (actionType) {
      case "EMAIL_INPUT":
        this.setState({
          user: {
            ...this.state.user,
            email: text,
          },
        });
        break;
      case "PASSWORD_INPUT":
        this.setState({
          user: {
            ...this.state.user,
            password: text,
          },
        });
        break;
      case "LOGIN":
        const email = await AsyncStorage.getItem(this.state.user.email);
        const pass = email !== null ? email : this.setState({ show_toast: true, message: "Email not found" });
        if (pass === this.state.user.password) {
          await AsyncStorage.setItem("isSignedIn", "true");
          this.props.isSignedIn(await AsyncStorage.getItem("isSignedIn"));
        } else {
          this.setState({ show_toast: true, message: "Invalid Password" });
        }
        break;
      case "GO_TO_SIGNUP":
        this.props.navigation.navigate("Signup");
        break;
      default:
        break;
    }
  };

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <SafeAreaView style={styles.container}>
          <View style={styles.loginTitleContainer}>
            <Text style={styles.loginTitle}>Login</Text>
            <Text>Please login to continue</Text>
          </View>
          <View style={styles.fieldsContainer}>
            <View style={styles.field}>
              <Text style={styles.iconContainer}>
                <Icon name="user" style={styles.icon} size={20} color="black" />
              </Text>
              <TextInput
                name="email"
                style={[styles.input, { width: "100%" }]}
                onChangeText={(text) => {
                  this.handleAction("EMAIL_INPUT", text);
                }}
                returnKeyType="next"
                returnKeyLabel="next"
                onSubmitEditing={() => {
                  this.refs.pass.focus();
                }}
                autoCapitalize="none"
                placeholder="Email"
                blurOnSubmit={false}
              />
            </View>

            <View style={styles.field}>
              <Text style={styles.iconContainer}>
                <Icon name="lock" style={styles.icon} size={20} color="black" />
              </Text>
              <TextInput
                name="password"
                ref="pass"
                style={[styles.input, { width: "80%" }]}
                onChangeText={(text) => {
                  this.handleAction("PASSWORD_INPUT", text);
                }}
                returnKeyType="done"
                returnKeyLabel="done"
                onSubmitEditing={() => {
                  this.handleAction("LOGIN");
                }}
                autoCapitalize="none"
                placeholder="Password"
                secureTextEntry={true}
                blurOnSubmit={false}
              />
              <View style={styles.forgotContainer}>
                <Text style={styles.forgotLabel}>Forgot</Text>
              </View>
            </View>
          </View>

          <View style={styles.loginButtonContainer}>
            <TouchableOpacity onPress={() => this.handleAction("LOGIN")}>
              <LinearGradient colors={["#FFA500", "#ffdb99"]} style={styles.loginButton} onPress={() => this.handleAction("LOGIN")}>
                <Text style={{ fontSize: 15, color: "white" }}>Login</Text>
                <Icon name="arrow-right" style={[styles.icon, { paddingLeft: 10, color: "white" }]} size={15} color="black" />
              </LinearGradient>
            </TouchableOpacity>
          </View>

          <View style={styles.footer}>
            <Text>Don't have an account? </Text>
            <TouchableOpacity onPress={() => this.handleAction("GO_TO_SIGNUP")} activeOpacity={0.6}>
              <Text style={{ fontSize: 15, color: "orange" }}>Signup</Text>
            </TouchableOpacity>
          </View>

          <Toast visible={this.state.show_toast} message={this.state.message} />
        </SafeAreaView>
      </TouchableWithoutFeedback>
    );
  }
}

export const mapDispatchToProps = (dispatch) => {
  return {
    isSignedIn: (status) => dispatch({ type: Types.IS_SIGNED_IN, payload: status }),
  };
};
export default connect(null, mapDispatchToProps)(Login);
