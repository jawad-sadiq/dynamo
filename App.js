import React, { Component } from "react";
import DynamoApp from "./src/root";

export default class App extends Component {
  render() {
    return <DynamoApp />;
  }
}
